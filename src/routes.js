const express = require('express')

const routes = express.Router()

const PartesController = require('./controllers/PartesController')
const ContratosController = require('./controllers/ContratosController')

routes
    //Rotas de Partes
    .get('/partes', PartesController.index)
    .post('/partes', PartesController.create)
    .put('/partes/:id', PartesController.update)
    .delete('/partes/:id', PartesController.delete)

    //Rotas de Contratos
    .get('/contratos', ContratosController.index)
    .post('/contratos/search', ContratosController.search)
    .post('/contratos', ContratosController.create)
    .post('/contratos/partes', ContratosController.partesContratos)
    .get('/contratos/partes/list/:id', ContratosController.listPartesContartos)
    .delete('/contratos/:id', ContratosController.delete)


module.exports  = routes