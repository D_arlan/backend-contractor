const knex = require('../database')

module.exports = {
    async index(req, res, next){
        try {
            const {cpf} = req.query
            const query = knex('partes')
            
            if(cpf){
                query
                .where({cpf})
            }
            const results = await query
            
            return res.json(results)
        } catch (error) {
            next(error)
        }
        
    },
    async create(req, res, next){
        try {
            const {nome, sobrenome, cpf, telefone} = req.body
            await knex('partes').insert({nome, sobrenome, cpf, telefone}) 
            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },
    async update(req, res, next){
        try {
            const {nome, sobrenome, cpf, telefone} = req.body
            const {id} = req.params
            await knex('partes')
            .update({nome, sobrenome, cpf, telefone})
            .where({id})
            return res.send()
        } catch (error) {
            next(error)
        }
    },
    async delete(req, res, next){
        try {
            const {id} = req.params
            await knex('partes')
            .where({id})
            .del()
            return res.send()
        } catch (error) {
            next(error)
        }
    }
}