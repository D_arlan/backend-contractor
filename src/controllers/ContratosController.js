const knex = require('../database')

module.exports = {
    async index(req, res, next){
        try {
            const {id, cpf} = req.query

            const query = knex('partes_contratos')
            
            if(id){
                query
                .join('contratos', 'contratos.id', '=', 'partes_contratos.contrato_id')
                .join('partes', 'partes.id', '=', 'partes_contratos.parte_id')
                .where('contratos.id', id)
                .select(
                    'partes_contratos.*', 
                    'contratos.titulo', 
                    'contratos.datainicial', 
                    'contratos.vencimento',
                    'contratos.id_parte',
                    'partes.nome',
                    'partes.sobrenome',
                    'partes.cpf',
                    'partes.telefone',
                    'partes.id'
                )
            }
            else{
                query
                .join('contratos', 'contratos.id', '=', 'partes_contratos.contrato_id')
                .join('partes', 'partes.id', '=', 'partes_contratos.parte_id')
                .where('cpf', cpf)
                .select(
                    'partes_contratos.*', 
                    'contratos.titulo', 
                    'contratos.datainicial', 
                    'contratos.vencimento',
                    'contratos.id_parte',
                    'partes.nome',
                    'partes.sobrenome',
                    'partes.cpf',
                    'partes.telefone'
                ).groupBy('contratos.id')
            }
            const results = await query
            return res.json(results)
        } catch (error) {
            next(error)
        }
        
    },
    async search(req, res, next){
        try {
            const {p} = req.query
            const {id_parte} = req.body

            const query = knex('contratos')
            .join('partes_contratos', 'contratos.id', '=', 'partes_contratos.contrato_id')
            .join('partes', 'partes.id', '=', 'partes_contratos.parte_id')
            .where('titulo', 'like', `%${p}%`)
            .where('id_parte', '=', id_parte)
            .select(
                'partes_contratos.*', 
                'contratos.titulo', 
                'contratos.datainicial', 
                'contratos.vencimento',
                'partes.nome',
                'partes.sobrenome',
                'partes.cpf',
                'partes.telefone'
            )
            
            
            
            const results = await query
            return res.json(results)
        } catch (error) {
            next(error)
        }
    },
    async create(req, res, next){
        try {
            await knex.transaction(async trx =>{
                const {titulo, datainicial, vencimento, id_parte, include_id} = req.body
                await knex('contratos').transacting(trx)
                .insert({titulo, datainicial, vencimento, id_parte})
                .returning('id')
                .then(async function(resp){
                        const id = resp[0];
                        console.log(id);
                        await knex('partes_contratos').transacting(trx)
                        .insert({parte_id:id_parte, contrato_id:id})
                        return res.status(201).send()
                })
            })        
        } catch (error) {
            next(error)
        }
    },
    async partesContratos(req, res, next){
        const {contrato_id, cpf} = req.body
        try {
            const parte = await knex('partes').where({cpf})
            //console.log(parte[0].id);
            await knex('partes_contratos').insert({parte_id:parte[0].id, contrato_id})
            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },
    async listPartesContartos(req, res, next){
        const {id} = req.params
        try {
            const results = await knex('partes_contratos')
            .join('partes', 'partes.id', '=', 'partes_contratos.parte_id')
            .where('contrato_id', id)
            .select(
                'partes_contratos.*', 
                'partes.nome',
                'partes.sobrenome',
                'partes.cpf',
                'partes.telefone'
            ).groupBy('partes.id')


            res.json(results)
        } catch (error) {
            next(error)
        }
            
    },
    async delete(req, res, next){
        try {
            const {id} = req.params
            await knex.transaction(async trx =>{
                await knex('contratos').transacting(trx)
                .where({id})
                .del()
                .then(async function(resp){
                    await knex('partes_contratos').transacting(trx)
                    .where({contrato_id:id})
                    .del()
                    return res.send()
                })
            })
        } catch (error) {
            next(error)
        }
    }
}