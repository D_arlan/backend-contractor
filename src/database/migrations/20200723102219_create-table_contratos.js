
exports.up = knex => knex.schema.createTable('contratos', table =>{
    table.increments();
    table.string('titulo').notNullable();
    table.string('datainicial').notNullable();
    table.integer('vencimento').notNullable();
    table.integer('id_parte')
         .notNullable()
         .references('id')
         .inTable('partes')
    
    table.timestamp(true, true)
  })
;
exports.down = knex => knex.schema.dropTable('contratos');
