
exports.up = knex => knex.schema.createTable('partes_contratos', table =>{
        table.increments()
        table
          .integer('parte_id')
          .notNullable()
          .references('id')
          .inTable('partes')
        table
          .integer('contrato_id')
          .notNullable()
          .references('id')
          .inTable('contratos')
})

exports.down = knex => knex.schema.dropTable('partes_contratos');
