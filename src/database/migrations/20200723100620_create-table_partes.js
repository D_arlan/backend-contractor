
exports.up = knex => knex.schema.createTable('partes', table =>{
        table.increments();
        table.string('nome').notNullable();
        table.string('sobrenome').notNullable();
        table.integer('cpf').unique().notNullable();
        table.integer('telefone').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
      })
;
exports.down = knex => knex.schema.dropTable('partes');
