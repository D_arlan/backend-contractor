
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('partes').del()
    .then(function () {
      // Inserts seed entries
      return knex('partes').insert([
        {
          nome: 'Cosme',
          sobrenome: 'Darlan',
          cpf:60874717310,
          telefone:85999286979
        },
        {
          nome: 'Joe',
          sobrenome: 'Silva',
          cpf:60874717320,
          telefone:85999286979
        },
        {
          nome: 'Mario',
          sobrenome: 'Andrade',
          cpf:60874717330,
          telefone:85999286979
        }       
      ]);
    });
};
