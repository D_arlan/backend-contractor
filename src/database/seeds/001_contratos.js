
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('contratos').del()
    .then(function () {
      // Inserts seed entries
      return knex('contratos').insert([
        {
          titulo: 'Contrato casa 1',
          datainicial: '2020-07-23 02:29:34',
          vencimento:'2020-07-23 02:29:34',
        },
        {
          titulo: 'Contrato casa 2',
          datainicial: '2020-07-23 02:29:34',
          vencimento:'2020-07-23 02:29:34',
        },
        {
          titulo: 'Contrato casa 3',
          datainicial: '2020-07-23 02:29:34',
          vencimento:'2020-07-23 02:29:34',
        }
      ]);
    });
};
