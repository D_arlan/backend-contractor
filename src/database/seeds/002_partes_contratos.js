
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('partes_contratos').del()
    .then(function () {
      // Inserts seed entries
      return knex('partes_contratos').insert([
        {
          parte_id: 4,
          contrato_id:1
        },
        {
          parte_id: 4,
          contrato_id:2
        }
      ]);
    });
};
